package com.nvm.minh.androidmock.view

import android.app.*
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.adapters.CastAndCrewAdapter
import com.nvm.minh.androidmock.api.ApiInterface
import com.nvm.minh.androidmock.api.RetrofitClient
import com.nvm.minh.androidmock.broadcastreceiver.AlarmReceiver
import com.nvm.minh.androidmock.constant.APIConstant
import com.nvm.minh.androidmock.constant.Constant
import com.nvm.minh.androidmock.database.DatabaseOpenHelper
import com.nvm.minh.androidmock.listener.BadgeListener
import com.nvm.minh.androidmock.listener.DetailListener
import com.nvm.minh.androidmock.listener.MovieListener
import com.nvm.minh.androidmock.model.CastAndCrew
import com.nvm.minh.androidmock.model.CastCrewList
import com.nvm.minh.androidmock.model.Movie
import com.nvm.minh.androidmock.util.NotificationUtil
import com.squareup.picasso.Picasso

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

const val notificationID = 1
const val channelID = "channel1"
const val titleExtra = "titleExtra"
const val messageExtra = "messageExtra"

class DetailFragment () : Fragment(), View.OnClickListener {
    private var mMovie = Movie(0, "0", "0", 0.0, "0", "0", false, false, "0", "0")
    //private lateinit var mMovie: Movie
    private lateinit var mCastAndCrewList: ArrayList<CastAndCrew>
    private lateinit var mFavoriteBtn: ImageButton
    private lateinit var mDateText: TextView
    private lateinit var mRateText: TextView
    private lateinit var mPosterImg: ImageView
    private lateinit var mReminderBtn: Button
    private lateinit var mReminderTimeText: TextView
    private lateinit var mOverviewText: TextView
    private lateinit var mCastRecyclerView: RecyclerView
    private lateinit var mCastAndCrewAdapter: CastAndCrewAdapter

    private lateinit var mDatabaseOpenHelper: DatabaseOpenHelper
    private lateinit var mBadgeListener: BadgeListener
    private lateinit var mDetailListener: DetailListener
    private lateinit var mMovieListener: MovieListener

    private var mReminderExist: Boolean = false

    private var mSaveDay = 0
    private var mSaveMonth = 0
    private var mSaveYear = 0
    private var mSaveHour = 0
    private var mSaveMinute = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    fun setBadgeListener(badgeListener: BadgeListener) {
        this.mBadgeListener = badgeListener
    }

    fun setDetailListener(detailListener: DetailListener) {
        this.mDetailListener = detailListener
    }

    fun setMovieListener(movieListener: MovieListener) {
        this.mMovieListener = movieListener
    }

    fun setDatabaseOpenHelper(databaseOpenHelper: DatabaseOpenHelper) {
        this.mDatabaseOpenHelper = databaseOpenHelper
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        val bundle = this.arguments
        //mMovie = bundle?.getSerializable("movieDetail") as Movie
        if (bundle != null) {
            mMovie = bundle.getSerializable("movieDetail") as Movie
            val movieReminderList = mDatabaseOpenHelper.getListReminder()
            for (i in 0 until movieReminderList.size) {
                if (mMovie.id == movieReminderList[i].id) {
                    mReminderExist = true
                    break
                }
            }
        }

        mFavoriteBtn = view.findViewById(R.id.favorite_img_btn)
        mDateText = view.findViewById(R.id.release_date_text)
        mRateText = view.findViewById(R.id.rate_text)
        mPosterImg = view.findViewById(R.id.movie_poster_img)
        mOverviewText = view.findViewById(R.id.overview_text)
        mReminderBtn = view.findViewById(R.id.reminder_btn)
        mReminderTimeText = view.findViewById(R.id.reminder_time_text)
        mCastRecyclerView = view.findViewById(R.id.recycler_view_cast)
        if (mMovie.isFavorite) {
            mFavoriteBtn.setImageResource(R.drawable.ic_baseline_star_24)
        } else {
            mFavoriteBtn.setImageResource(R.drawable.ic_baseline_star_outline_24)
        }
        mFavoriteBtn.setOnClickListener(this)
        mDateText.text = mMovie.releaseDate
        mRateText.text = "${mMovie.voteAverage}/10"
        val url = APIConstant.BASE_IMG_URL + mMovie.posterPath
        Picasso.get().load(url).into(mPosterImg)
        mReminderBtn.setOnClickListener{
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                createReminder()
            }
        }
        mOverviewText.text = mMovie.overview
        if (mReminderExist) {
            mReminderTimeText.text = mMovie.reminderTimeDisplay
        } else {
            mReminderTimeText.visibility = View.GONE
        }
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        mCastAndCrewList = ArrayList()
        mCastAndCrewAdapter = CastAndCrewAdapter(mCastAndCrewList)
        mCastRecyclerView.adapter = mCastAndCrewAdapter
        mCastRecyclerView.layoutManager = layoutManager
        getCastAndCrewFromApi()

        setHasOptionsMenu(true)

        return view

    }

    private fun createReminder() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(requireContext(), { _, year, month, day ->
            TimePickerDialog(requireContext(), { _, hour, minute ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day, hour, minute)
                mSaveYear = year
                mSaveMonth = month
                mSaveDay = day
                mSaveHour = hour
                mSaveMinute = minute
                currentDateTime.set(mSaveYear, mSaveMonth, mSaveDay, mSaveHour, mSaveMinute)
                val time: Long = currentDateTime.timeInMillis
                val timeHour = "$mSaveYear-${mSaveMonth + 1}-$mSaveDay $mSaveHour:$mSaveMinute"
                mReminderTimeText.visibility = View.VISIBLE
                mReminderTimeText.text = timeHour

                mMovie.reminderTimeDisplay = timeHour
                mMovie.reminderTime = time.toString()
                if (mDatabaseOpenHelper.checkReminderExist(mMovie.id) > 0) {
                    if (mDatabaseOpenHelper.updateReminder(mMovie) > 0) {
                        NotificationUtil().cancelNotification(notificationID, requireContext())
                        Toast.makeText(context, "Update reminder successfully", Toast.LENGTH_SHORT).show()
                        createNotification(time)
                    } else {
                        Toast.makeText(context, "Update reminder fail", Toast.LENGTH_SHORT).show()
                    }
                    //Toast.makeText(context, "This reminder is already exist", Toast.LENGTH_SHORT).show()

                } else {
                    if (mDatabaseOpenHelper.addReminder(mMovie) > 0) {
                        Toast.makeText(context, "Add reminder successfully", Toast.LENGTH_SHORT).show()
                        createNotification(time)
                        mDetailListener.onAddReminder()
                    } else {
                        Toast.makeText(context, "Add reminder fail", Toast.LENGTH_SHORT).show()
                    }
                }
            }, startHour, startMinute, true).show()
        }, startYear, startMonth, startDay).show()
    }

    private fun createNotification(time: Long) {
        createNotificationChannel()
        val intent = Intent(context?.applicationContext, AlarmReceiver::class.java)
//        val title = mMovie.title
//        val release = mMovie.releaseDate
//        val rate = mMovie.voteAverage
//        val message = "The movie reminder has arrived"
//        intent.putExtra(titleExtra, title)
//        intent.putExtra(messageExtra, message)
        val bundle = Bundle()
        bundle.putInt(Constant.BUNDLE_ID_KEY, mMovie.id)
        bundle.putString(Constant.BUNDLE_TITLE_KEY, mMovie.title)
        bundle.putString(Constant.BUNDLE_RELEASE_KEY, mMovie.releaseDate)
        bundle.putDouble(Constant.BUNDLE_RATING_KEY, mMovie.voteAverage)
        //bundle.putString("time", mMovie.reminderTimeDisplay)
        intent.putExtras(bundle)
        val pendingIntent = PendingIntent.getBroadcast(
            activity?.applicationContext, notificationID, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager = activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent)
        }
    }

    private fun createNotificationChannel() {
        val channel = NotificationChannel(channelID, "Movie!!!", NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = "Time to watch a movie <3"
        val notificationManager = activity?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.change_layout)
        item.isVisible = false
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.favorite_img_btn -> {
                if (mMovie.isFavorite) {
                    if (mDatabaseOpenHelper.deleteMovie(mMovie.id) > -1) {
                        mFavoriteBtn.setImageResource(R.drawable.ic_baseline_star_outline_24)
                        mMovie.isFavorite = false
                        mBadgeListener.onUpdateBadgeNumber(false)
                        mDetailListener.onUpdateFromDetail(mMovie, false)
                    }
                } else {
                        if (mDatabaseOpenHelper.addMovie(mMovie) > -1) {
                            mFavoriteBtn.setImageResource(R.drawable.ic_baseline_star_24)
                            mMovie.isFavorite = true
                            mBadgeListener.onUpdateBadgeNumber(true)
                            mDetailListener.onUpdateFromDetail(mMovie, true)
                        }
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        mMovieListener.onUpdateTitleMovie("Movies")
    }

    private fun getCastAndCrewFromApi() {
        val retrofit: ApiInterface =
            RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getCastAndCrew(mMovie.id, APIConstant.API_KEY)
        retrofitData.enqueue(object : Callback<CastCrewList> {
            override fun onResponse(call: Call<CastCrewList>?, response: Response<CastCrewList>?) {
                val responseBody = response?.body()
                mCastAndCrewList.addAll(responseBody!!.castList)
                mCastAndCrewList.addAll(responseBody.crewList)
                mCastAndCrewAdapter.updateList(mCastAndCrewList)
            }

            override fun onFailure(call: Call<CastCrewList>?, t: Throwable?) {
            }

        })
    }

    fun unFavoriteMovie(movieId: Int) {
        if (mMovie.id == movieId) {
            mMovie.isFavorite = false
            mFavoriteBtn.setImageResource(R.drawable.ic_baseline_star_outline_24)
        }
    }
}