package com.nvm.minh.androidmock.listener

interface SettingListener {
    fun onUpdateFromSettings(category: String, rate: String, releaseYear: String, sort: String)
}