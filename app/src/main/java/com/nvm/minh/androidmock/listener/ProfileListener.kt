package com.nvm.minh.androidmock.listener

import android.graphics.Bitmap

interface ProfileListener {
    fun onSaveProfile(name: String, email: String, dob: String, gender: String, imgBitmap: Bitmap?)
}