package com.nvm.minh.androidmock.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.constant.APIConstant
import com.nvm.minh.androidmock.model.Movie
import com.squareup.picasso.Picasso
import java.lang.Exception

class MovieAdapter(
    private var mListMovie: MutableList<Movie>,
    private var mScreenType: Int,
    private var mIsFavoriteList: Boolean,
    private var mViewClickListener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun updateData(listMovie: MutableList<Movie>) {
        this.mListMovie = listMovie
        //notifyDataSetChanged()
    }

    fun setViewType(screenType: Int) {
        this.mScreenType = screenType
    }

    fun settingMovieFavorite(listMovieFav: ArrayList<Movie>) {
        for (i in 0 until mListMovie.size) {
            for (j in 0 until listMovieFav.size) {
                if (mListMovie[i].id == listMovieFav[j].id) {
                    mListMovie[i].isFavorite = true
                }
            }
        }
    }

    fun setupMovieSettings(
        listMovie: MutableList<Movie>,
        rate: String,
        releaseYear: String,
        sortBy: String
    ) {
        val convertRate: Int? = try {
            rate.toInt()
        } catch (e: Exception) {
            null
        }
        if (convertRate != null) {
            listMovie.removeAll { it.voteAverage < convertRate.toDouble() }
        }

        val convertYear: Int? = if (releaseYear.length > 3) {
            releaseYear.substring(0, 4).trim().toIntOrNull()
        } else {
            null
        }
        if (convertYear != null) {
            //listMovie.removeAll { it.releaseDate.substring(0, 4).trim() != releaseYear }
            listMovie.removeAll {
                if (it.releaseDate.length > 4) {
                    it.releaseDate.substring(0, 4).trim() != releaseYear
                } else {
                    it.releaseDate != releaseYear
                }
            }
        }

        if (sortBy == "release") {
            listMovie.sortByDescending { it.releaseDate }
        } else if (sortBy == "rate") {
            listMovie.sortByDescending { it.voteAverage }
        }
        updateData(listMovie)
    }

    override fun getItemCount(): Int {
        return mListMovie.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 1) {
            ListViewHolder(
                mViewClickListener,
                mListMovie,
                LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
            )
        } else if (viewType == 0) {
            GridViewHolder(
                mListMovie,
                LayoutInflater.from(parent.context).inflate(R.layout.movie_item_grid, parent, false)
            )
        } else {
            LoadViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.movie_item_load, parent, false)
            )
        }

//        return ListViewHolder(mViewClickListener,
//            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false))

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.tag = position
        holder.itemView.setOnClickListener(mViewClickListener)
        if (holder is GridViewHolder) {
            holder.bindDataGrid(position)
        } else if (holder is ListViewHolder) {
            holder.bindDataList(position)
        }
    }

    class ListViewHolder(
        private var mViewClickListener: View.OnClickListener,
        private var movieList: MutableList<Movie>,
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        private var itemTitle = itemView.findViewById<TextView>(R.id.tv_title)
        private var itemImgMovie =  itemView.findViewById<ImageView>(R.id.img_movie)
        private var itemDate = itemView.findViewById<TextView>(R.id.tv_date)
        private var itemRate = itemView.findViewById<TextView>(R.id.tv_rate)
        private var itemAdult = itemView.findViewById<ImageView>(R.id.img_adult)
        private var itemAddFav = itemView.findViewById<ImageButton>(R.id.fav_btn)
        private var itemOverview = itemView.findViewById<TextView>(R.id.tv_overview)
        fun bindDataList(position: Int) {
            val movie = movieList[position]
            val url = APIConstant.BASE_IMG_URL + movie.posterPath
            Picasso.get().load(url).into(itemImgMovie)
            Log.d("posterPath", movie.posterPath)
            Log.d("adult", movie.adult.toString())
            itemTitle.text = movie.title
            itemDate.text = movie.releaseDate
            "${movie.voteAverage}/10".also { itemRate.text = it }
            itemOverview.text = movie.overview
            if (movie.adult) {
                itemAdult.visibility = View.VISIBLE
            } else {
                itemAdult.visibility = View.GONE
            }
            if (movie.isFavorite) {
                itemAddFav.setImageResource(R.drawable.ic_baseline_star_24)
            } else {
                itemAddFav.setImageResource(R.drawable.ic_baseline_star_outline_24)
            }
            itemAddFav.tag = position
            itemAddFav.setOnClickListener(mViewClickListener)
        }
    }

    class GridViewHolder(private var movieList: MutableList<Movie>, itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var imgMovie: ImageView = itemView.findViewById(R.id.img_movie_grid)
        private var tvTitle: TextView = itemView.findViewById(R.id.tv_title_grid)

        fun bindDataGrid(position: Int) {
            val movie = movieList[position]
            val url = APIConstant.BASE_IMG_URL + movie.posterPath
            Picasso.get().load(url).into(imgMovie)
            tvTitle.text = movie.title
        }
    }

    class LoadViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemViewType(position: Int): Int {
        if (!mIsFavoriteList && mListMovie.isNotEmpty() && position == mListMovie.size - 1) return 2
        else return mScreenType
    }

    fun removeItemLoading() {
        if (mListMovie.isNotEmpty()) {
            val lastPosition = mListMovie.size - 1
            mListMovie.removeAt(lastPosition)
            notifyDataSetChanged()
        }
    }

}