package com.nvm.minh.androidmock.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.adapters.ReminderAdapter
import com.nvm.minh.androidmock.database.DatabaseOpenHelper
import com.nvm.minh.androidmock.listener.ReminderListener
import com.nvm.minh.androidmock.model.Movie
import com.nvm.minh.androidmock.util.NotificationUtil

class ReminderFragment(private var mDatabaseOpenHelper: DatabaseOpenHelper) : Fragment(), ReminderListener {

    private lateinit var mReminderAdapter: ReminderAdapter
    private lateinit var mReminderRecyclerView: RecyclerView
    private lateinit var mReminderList: ArrayList<Movie>
    //private lateinit var mToolbarTitleListener: ToolbarTitleListener
    private lateinit var mReminderListener: ReminderListener

    fun setReminderListener(reminderListener: ReminderListener) {
        this.mReminderListener = reminderListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_reminder, container, false)

        mReminderList = mDatabaseOpenHelper.getListReminder()
        val linearLayoutManager = LinearLayoutManager(context)
        mReminderAdapter = ReminderAdapter(mReminderList, ReminderAdapter.REMINDER_ALL, mDatabaseOpenHelper)
        mReminderAdapter.setReminderListener(this)
        mReminderRecyclerView = view.findViewById(R.id.recycler_view_all_reminder)
        mReminderRecyclerView.layoutManager = linearLayoutManager
        mReminderRecyclerView.setHasFixedSize(true)
        mReminderRecyclerView.adapter = mReminderAdapter
        mReminderAdapter.updateData(mReminderList)
        setHasOptionsMenu(true)

        return view
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.change_layout)
        item.isVisible = false
    }

    override fun onLoadReminder() {
        TODO("Not yet implemented")
    }

    override fun onShowDetailReminder(movie: Movie) {
        mReminderListener.onShowDetailReminder(movie)
    }

    override fun onLongClickReminder(position: Int): Boolean {
        showArlertDialog(position)
        return false
    }

    private fun showArlertDialog(position: Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Confirm action")
            .setMessage("Do you want to delete this reminder?")
            .setNegativeButton("No", null)
            .setPositiveButton("Yes") { _, _ ->
                NotificationUtil().cancelNotification(notificationID, requireContext())
                mReminderAdapter.deleteItem(position)
                mReminderAdapter.notifyDataSetChanged()
                mReminderListener.onLoadReminder()

                if (mReminderList.size <= 0) {
                    activity?.supportFragmentManager!!.beginTransaction().remove(this).commit()
                }
            }
            .show()
    }
}