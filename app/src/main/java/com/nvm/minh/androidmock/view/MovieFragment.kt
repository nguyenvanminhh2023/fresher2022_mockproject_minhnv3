package com.nvm.minh.androidmock.view

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.adapters.MovieAdapter
import com.nvm.minh.androidmock.api.ApiInterface
import com.nvm.minh.androidmock.api.RetrofitClient
import com.nvm.minh.androidmock.constant.APIConstant
import com.nvm.minh.androidmock.constant.Constant
import com.nvm.minh.androidmock.database.DatabaseOpenHelper
import com.nvm.minh.androidmock.listener.BadgeListener
import com.nvm.minh.androidmock.listener.DetailListener
import com.nvm.minh.androidmock.listener.MovieListener
import com.nvm.minh.androidmock.model.Movie
import com.nvm.minh.androidmock.model.MovieList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieFragment() : Fragment(), View.OnClickListener {
    //private val mMovieViewModel: MovieViewModel by activityViewModels()

    private var mScreenType: Int = 1
    private var mMovieList = ArrayList<Movie>()
    private var mDetailFragment = DetailFragment()
    private lateinit var mMovieRecyclerView: RecyclerView
    private lateinit var mMovieAdapter: MovieAdapter
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mGridLayoutManager: GridLayoutManager
    private lateinit var mDatabaseOpenHelper: DatabaseOpenHelper
    private lateinit var mMovieListDB: ArrayList<Movie>
    private lateinit var mProgressBar: ProgressBar
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    private var mCurrentPage: Int = 0

    private var mCategoryPref: String = "popular"
    private var mRatePref: String = ""
    private var mReleaseYearPref: String = ""
    private var mSortPref: String = ""

    private lateinit var mBadgeListener: BadgeListener
    private lateinit var mMovieListener: MovieListener

    fun setScreenType(screenType : Int) {
        this.mScreenType = screenType
    }

    fun getScreenType() : Int {
        return this.mScreenType
    }

    fun setBadgeListener(badgeListener: BadgeListener) {
        this.mBadgeListener = badgeListener
        mDetailFragment.setBadgeListener(badgeListener)
    }

    fun setMovieListener(movieListener: MovieListener) {
        this.mMovieListener = movieListener
        mDetailFragment.setMovieListener(movieListener)
    }

    fun setDetailListener(detailListener: DetailListener){
        mDetailFragment.setDetailListener(detailListener)
    }

    fun setCategorySettings(category: String) {
        this.mCategoryPref = category
    }

    fun setRateSettings(rate: String) {
        this.mRatePref = rate
    }

    fun setReleaseYearSettings(releaseYear: String) {
        this.mReleaseYearPref = releaseYear
    }

    fun setSortSettings(sort: String) {
        this.mSortPref = sort
    }

    fun setListMovieByCondition(
        categoryPref: String,
        ratePref: String,
        releaseYearPref: String,
        sortPref: String
    ) {
        this.mCurrentPage = 1
        mMovieList.clear()
        this.mCategoryPref = categoryPref
        this.mRatePref = ratePref
        this.mReleaseYearPref = releaseYearPref
        this.mSortPref = sortPref
        getListMovieFromApi(false, false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_movie, container, false)
//        mMovieAdapter = MovieAdapter(mMovieList, mScreenType,false, this)
        mMovieRecyclerView = view.findViewById(R.id.list_recycleView)
        mDatabaseOpenHelper = activity?.let { DatabaseOpenHelper(it, "movie_database", null, 1) }!!
        mDetailFragment.setDatabaseOpenHelper(mDatabaseOpenHelper)
        mMovieListDB = mDatabaseOpenHelper.getListMovie()
        mProgressBar = view.findViewById(R.id.progress_bar_layout)
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_layout)

        updateMovieList()

        getListMovieFromApi(false, false)
        mSwipeRefreshLayout.setOnRefreshListener {
            mSwipeRefreshLayout.isRefreshing = false
            updateMovieList()
            getListMovieFromApi(false, true)
        }
        onLoadMoreListener()

//        mMovieViewModel.movieList.observe(requireActivity(), {
//            mMovieList.addAll(it)
//            mMovieAdapter.notifyDataSetChanged()
//        })
//
//        mMovieViewModel.movieListDB.observe(requireActivity(), {
//            mMovieListDB = it
//        })

//        val mApiData = ApiData()
//        mApiData.getListMovieFromApi(mPage,"popular", mMovieList, mMovieListDB, mMovieAdapter)
        return view
    }

    private fun updateMovieList() {
        mCurrentPage = 1
        mMovieList = ArrayList()
        mLinearLayoutManager = LinearLayoutManager(activity)
        mGridLayoutManager = GridLayoutManager(activity, 2)
        mMovieAdapter = MovieAdapter(mMovieList, mScreenType,false, this)
        if (mScreenType == 1) {
            mMovieRecyclerView.layoutManager = mLinearLayoutManager
        } else {
            mMovieRecyclerView.layoutManager = mGridLayoutManager
        }
        mMovieRecyclerView.adapter = mMovieAdapter

    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.fav_btn -> {
                var position = view.tag as Int
                val movieItem = mMovieList[position]
                if (movieItem.isFavorite) {
                    if (mDatabaseOpenHelper.deleteMovie(movieItem.id) > -1) {
                        movieItem.isFavorite = false
                        mMovieAdapter.notifyItemChanged(position)
                        mBadgeListener.onUpdateBadgeNumber(false)
                        mMovieListener.onUpdateFromMovie(movieItem, false)
                    }
                } else {
                    if (mDatabaseOpenHelper.addMovie(movieItem) > -1) {
                        movieItem.isFavorite = true
                        mMovieAdapter.notifyItemChanged(position)
                        mBadgeListener.onUpdateBadgeNumber(true)
                        mMovieListener.onUpdateFromMovie(movieItem, true)
                    }
                }
            }
            R.id.movie_item -> {
                val position = view.tag as Int
                val movieItem: Movie = mMovieList[position]
                val bundle = Bundle()
                bundle.putSerializable("movieDetail", movieItem)
                mDetailFragment.arguments = bundle
//                activity?.supportFragmentManager!!.beginTransaction().apply {
//                    add(R.id.frg_home, mMovieDetailFragment, "TAGTAGTAG")
//                    addToBackStack(null)
//                    commit()
//                }

                val transaction = requireFragmentManager().beginTransaction()
                transaction.add(R.id.frg_home, mDetailFragment, Constant.FRAGMENT_DETAIL_TAG)
                transaction.addToBackStack(null)
                transaction.commit()
                mMovieListener.onUpdateTitleMovie(movieItem.title)
            }
        }
    }
    companion object {

        @JvmStatic
        fun newInstance(mScreenType : Int) = MovieFragment().apply {
            arguments = Bundle().apply {
                putInt("screentype", mScreenType)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getInt("screentype")?.let { mScreenType = it }
    }

    fun changeViewHome() {
        if (mMovieRecyclerView.layoutManager == mGridLayoutManager) {
            mMovieRecyclerView.layoutManager = mLinearLayoutManager
            mMovieAdapter = MovieAdapter(mMovieList, 1, false, this)
            setScreenType(1)
        }
        else {
            mMovieRecyclerView.layoutManager = mGridLayoutManager
            mMovieAdapter = MovieAdapter(mMovieList, 0, false, this)
            setScreenType(0)
        }
        mMovieRecyclerView.adapter = mMovieAdapter
        mMovieAdapter.notifyDataSetChanged()
    }

    fun updateMovieList(movie: Movie, isFavorite:Boolean) {
        var position = -1
        val size = mMovieList.size
        for (i in 0 until size) {
            if (mMovieList[i].id == movie.id) {
                position = i
            }
        }
        mMovieList[position].isFavorite = isFavorite
        mMovieAdapter.notifyDataSetChanged()
    }

    private fun onLoadMoreListener() {
        mMovieRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isLastItemDisplaying(recyclerView)) {
                    getListMovieFromApi(true, false)
                }
            }
        })
    }

    private fun isLastItemDisplaying(recyclerView: RecyclerView) : Boolean {
        if (recyclerView.adapter!!.itemCount != 0) {
            val lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager?)!!.findLastCompletelyVisibleItemPosition()
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.adapter!!.itemCount - 1) {
                return true
            }
        }
        return false
    }

    private fun getListMovieFromApi(isLoadMore: Boolean, isRefresh: Boolean) {
        if (isLoadMore) {
            mCurrentPage++
        } else {
            if (!isRefresh) {
                mProgressBar.visibility = View.VISIBLE
            }
        }
        val retrofit : ApiInterface = RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getMovieList(mCategoryPref, APIConstant.API_KEY, "$mCurrentPage")
        retrofitData.enqueue(object : Callback<MovieList> {
            override fun onResponse(call: Call<MovieList>?, response: Response<MovieList>?) {
                mMovieAdapter.removeItemLoading()
                val responseBody = response?.body()
                val listMovieResult = responseBody?.results as ArrayList<Movie>
                //mMovieViewModel.movieList.value = listMovieResult
                for (i in 0 until listMovieResult.size) {
                    if (!mMovieList.contains(listMovieResult[i]))
                        mMovieList.add(listMovieResult[i])
                }
                if (mCurrentPage < responseBody.totalPages) {
                    val loadmoreItem = Movie(0, "0", "0", 0.0, "0", "0", false, false)
                    mMovieList.add(loadmoreItem)
                }
                mMovieAdapter.setupMovieSettings(mMovieList, mRatePref, mReleaseYearPref, mSortPref)
                mMovieAdapter.updateData(mMovieList)
                mMovieAdapter.settingMovieFavorite(mMovieListDB)
                mMovieAdapter.notifyDataSetChanged()
                if (!isLoadMore && !isRefresh) {
                    mProgressBar.visibility = View.GONE
                }
                if (isRefresh) {
                    mSwipeRefreshLayout.isRefreshing = false
                }
            }

            override fun onFailure(call: Call<MovieList>?, t: Throwable?) {
                if (isLoadMore) {
                    mCurrentPage -= 1
                } else if (isRefresh) {
                    mSwipeRefreshLayout.isRefreshing = false
                } else {
                    mProgressBar.visibility = View.GONE
                }
            }

        })
    }

}
