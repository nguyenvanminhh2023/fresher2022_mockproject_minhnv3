package com.nvm.minh.androidmock.listener

import com.nvm.minh.androidmock.model.Movie

interface MovieListener {
    fun onUpdateFromMovie(movie: Movie, isFavorite: Boolean)
    fun onUpdateTitleMovie(movieTitle: String)
}