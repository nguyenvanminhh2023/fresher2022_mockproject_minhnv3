package com.nvm.minh.androidmock.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.constant.APIConstant
import com.nvm.minh.androidmock.model.CastAndCrew
import com.squareup.picasso.Picasso

class CastAndCrewAdapter (private var mCastAndCrewList: ArrayList<CastAndCrew>) :
    RecyclerView.Adapter<CastAndCrewAdapter.CastAndCrewViewHolder>() {
    fun updateList(castAndCrewList: ArrayList<CastAndCrew>) {
        this.mCastAndCrewList = castAndCrewList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastAndCrewViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cast_crew_item, parent, false)
        return CastAndCrewViewHolder(view)
    }

    override fun onBindViewHolder(holder: CastAndCrewViewHolder, position: Int) {
        holder.bindDataCastAndCrew(mCastAndCrewList[position])
    }

    override fun getItemCount(): Int {
        return mCastAndCrewList.size
    }

    class CastAndCrewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var tvName = itemView.findViewById<TextView>(R.id.cast_crew_item_text)
        private var img = itemView.findViewById<ImageView>(R.id.cast_crew_item_image)
        fun bindDataCastAndCrew(castAndCrew: CastAndCrew) {
            val url = APIConstant.BASE_IMG_URL + castAndCrew.profilePath
            Picasso.get().load(url).error(R.drawable.ic_baseline_person_24).into(img)
            tvName.text = castAndCrew.name
        }
    }

}