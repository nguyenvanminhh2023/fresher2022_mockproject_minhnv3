package com.nvm.minh.androidmock.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nvm.minh.androidmock.R

class HomeFragment(private var mMovieFragment: MovieFragment) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    fun setMovieFragment(movieFragment: MovieFragment) {
        this.mMovieFragment = movieFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val transaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.frg_home, mMovieFragment)
        transaction.commit()
    }

}