package com.nvm.minh.androidmock.listener

import com.nvm.minh.androidmock.model.Movie

interface ReminderListener {
    fun onLoadReminder()
    fun onShowDetailReminder(movie: Movie)
    fun onLongClickReminder(position: Int): Boolean
}