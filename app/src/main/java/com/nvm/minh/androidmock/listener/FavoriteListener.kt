package com.nvm.minh.androidmock.listener

import com.nvm.minh.androidmock.model.Movie

interface FavoriteListener {
    fun onUpdateFromFavorite(movie: Movie)
}