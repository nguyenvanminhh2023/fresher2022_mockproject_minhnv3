package com.nvm.minh.androidmock.view

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.preference.*
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.constant.Constant
import com.nvm.minh.androidmock.listener.SettingListener

class SettingsFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {
    private lateinit var mCategoryListPref: ListPreference
    private lateinit var mRateSeekBarPref: SeekBarPreference
    private lateinit var mReleaseYearEditTextPref: EditTextPreference
    private lateinit var mSortListPref: ListPreference

    private lateinit var mSettingListener: SettingListener

    fun setSettingListener(settingListener: SettingListener) {
        this.mSettingListener = settingListener
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences?.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.fragment_settings, rootKey)
        setHasOptionsMenu(true)
        mCategoryListPref = findPreference(Constant.PREF_CATEGORY_KEY)!!
        mRateSeekBarPref = findPreference(Constant.PREF_RATE_KEY)!!
        mReleaseYearEditTextPref = findPreference(Constant.PREF_RELEASE_KEY)!!
        mSortListPref = findPreference(Constant.PREF_SORT_KEY)!!

        val sharedPreferences = activity?.let { PreferenceManager.getDefaultSharedPreferences(it) }
        val category = sharedPreferences?.getString(Constant.PREF_CATEGORY_KEY, "Popular movies")
        val rate = sharedPreferences?.getInt(Constant.PREF_RATE_KEY, 0)
        val release = sharedPreferences?.getString(Constant.PREF_RELEASE_KEY, "")
        val sort = sharedPreferences?.getString(Constant.PREF_SORT_KEY, "")
        mCategoryListPref.summary = when (category) {
            "popular" -> "Popular movies"
            "top_rated" -> "Top rated movies"
            "upcoming" -> "Upcoming movies"
            "now_playing" -> "Now playing movies"
            else -> "Popular movies"
        }
        mRateSeekBarPref.summary = when (rate) {
            0 -> ""
            else -> rate.toString()
        }
        mReleaseYearEditTextPref.summary = release
        mSortListPref.summary = when (sort) {
            "none" -> ""
            "release" -> "Release date"
            "rate" -> "Rating"
            else -> ""
        }
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        val category = p0?.getString(Constant.PREF_CATEGORY_KEY, "popular")
        val rate = p0?.getInt(Constant.PREF_RATE_KEY, 0)
        val releaseYear = p0?.getString(Constant.PREF_RELEASE_KEY, "")
        val sort = p0?.getString(Constant.PREF_SORT_KEY, "")

        when {
            p1.equals(Constant.PREF_CATEGORY_KEY) -> {
                mCategoryListPref.summary = when (category) {
                    "popular" -> "Popular movies"
                    "top_rated" -> "Top rated movies"
                    "upcoming" -> "Upcoming movies"
                    "now_playing" -> "Now playing movies"
                    else -> "Popular movies"
                }
            }
            p1.equals(Constant.PREF_RATE_KEY) -> {
                if (rate != 0) {
                    mRateSeekBarPref.summary = rate.toString()
                } else {
                    mRateSeekBarPref.summary = ""
                }
            }
            p1.equals(Constant.PREF_RELEASE_KEY) -> {
                if (!releaseYear.isNullOrEmpty()) {
                    mReleaseYearEditTextPref.summary = releaseYear
                } else {
                    mReleaseYearEditTextPref.summary = ""
                }
            }
            p1.equals(Constant.PREF_SORT_KEY) -> {
                if (sort != "none") {
                    if (sort.isNullOrEmpty()) {
                        mSortListPref.summary = ""
                    } else {
                        if (sort == "release") {
                            mSortListPref.summary = "Release date"
                        } else {
                            mSortListPref.summary = "Rating"
                        }
                    }
                } else {
                    mSortListPref.summary = ""
                }
            }
        }
        mSettingListener.onUpdateFromSettings(category!!, rate.toString(), releaseYear!!, sort!!)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.change_layout)
        item.isVisible = false
    }
}