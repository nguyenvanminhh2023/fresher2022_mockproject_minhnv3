package com.nvm.minh.androidmock.constant

class Constant {
    companion object{
        const val BUNDLE_TITLE_KEY = "movie_title"
        const val BUNDLE_RELEASE_KEY = "movie_release"
        const val BUNDLE_RATING_KEY = "movie_rate"
        const val BUNDLE_ID_KEY = "movie_id"
        const val FRAGMENT_REMINDER_TAG = "frg_reminder_tag"
        const val FRAGMENT_DETAIL_TAG = "frg_detail_tag"

        const val PREF_CATEGORY_KEY = "category_key"
        const val PREF_RATE_KEY = "rate_key"
        const val PREF_RELEASE_KEY = "release_key"
        const val PREF_SORT_KEY = "sort_key"
    }
}
