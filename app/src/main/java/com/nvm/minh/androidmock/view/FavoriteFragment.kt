package com.nvm.minh.androidmock.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.adapters.MovieAdapter
import com.nvm.minh.androidmock.database.DatabaseOpenHelper
import com.nvm.minh.androidmock.listener.BadgeListener
import com.nvm.minh.androidmock.listener.FavoriteListener
import com.nvm.minh.androidmock.model.Movie
import com.nvm.minh.androidmock.viewmodel.MovieViewModel

class FavoriteFragment(
    private var mDatabaseOpenHelper: DatabaseOpenHelper,
    private var mMovieFavoriteList: ArrayList<Movie>
    ) : Fragment(), View.OnClickListener {
    //private val mMovieViewModel: MovieViewModel by activityViewModels()

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mMovieAdapter: MovieAdapter
    //private lateinit var mMovieList : ArrayList<Movie>
    private var mCountFavoriteMovie: Int = 0
    private lateinit var mBadgeListener: BadgeListener
    private lateinit var mFavoriteListener: FavoriteListener

    fun setBadgeListener(badgeListener: BadgeListener) {
        this.mBadgeListener = badgeListener
    }

    fun setFavoriteListener(favoriteListener: FavoriteListener) {
        this.mFavoriteListener = favoriteListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_favorite, container, false)
        //mMovieList = mDatabaseOpenHelper.getListMovie()
        //mMovieAdapter = MovieAdapter(mMovieFavoriteList, 1, true, this)
        mRecyclerView = view.findViewById(R.id.fav_recycler_view)
        updateFavoriteList()
//        mMovieViewModel.movieListDB.value = mMovieFavoriteList
//        mMovieViewModel.movieListDB.observe(requireActivity(), {
//            mMovieFavoriteList = it
//            updateFavoriteList()
//        })

        mCountFavoriteMovie = mMovieFavoriteList.size
        mMovieAdapter.updateData(mMovieFavoriteList)
        mMovieAdapter.notifyDataSetChanged()
        setHasOptionsMenu(true)
        return view
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.fav_btn -> {
                var position = view.tag as Int
                val movieItem = mMovieFavoriteList[position]
                if (movieItem.isFavorite) {
                    if (mDatabaseOpenHelper.deleteMovie(movieItem.id) > -1) {
                        movieItem.isFavorite = false
                        mMovieFavoriteList.remove(movieItem)
                        mMovieAdapter.notifyDataSetChanged()
                        mBadgeListener.onUpdateBadgeNumber(false)
                        mFavoriteListener.onUpdateFromFavorite(movieItem)
                    }
                }
            }
        }
    }

    fun updateFavoriteList(movie: Movie, isFavorite:Boolean) {
        var position = -1
        val size = mMovieFavoriteList.size
        for (i in 0 until size) {
            if (mMovieFavoriteList[i].id == movie.id) {
                position = i
            }
        }
        if (isFavorite) {
            mMovieFavoriteList.add(movie)
        } else {
            if (position != -1) {
                mMovieFavoriteList.removeAt(position)
            }
        }
        updateFavoriteList()
        mMovieAdapter.updateData(mMovieFavoriteList)
        mMovieAdapter.notifyDataSetChanged()
    }

    private fun updateFavoriteList() {
        mMovieAdapter = MovieAdapter(mMovieFavoriteList, 1, true, this)
        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mMovieAdapter
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.change_layout)
        item.isVisible = false
    }

}
