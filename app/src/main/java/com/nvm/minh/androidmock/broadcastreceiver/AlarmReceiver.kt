package com.nvm.minh.androidmock.broadcastreceiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.constant.Constant
import com.nvm.minh.androidmock.database.DatabaseOpenHelper

const val notificationID = 1
const val channelID = "channel1"
const val titleExtra = "titleExtra"
const val messageExtra = "messageExtra"
class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val bundle = intent.extras
        if (bundle != null) {
            val id = bundle.getInt(Constant.BUNDLE_ID_KEY)
            //val time = bundle.getString("time")
            val title = bundle.getString(Constant.BUNDLE_TITLE_KEY)
            val release = bundle.getString(Constant.BUNDLE_RELEASE_KEY)
            var ratingtmp = bundle.getDouble(Constant.BUNDLE_RATING_KEY)
            var rating = ""
            "${ratingtmp}/10".also { rating = it }
            val notification = NotificationCompat.Builder(context, channelID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText("Release: " + release + "  Rate: " + rating)
                .build()
            val databaseOpenHelper = DatabaseOpenHelper(context, "movie_database",null,1)
            databaseOpenHelper.deleteReminder(id)
            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.notify(notificationID, notification)
        }
    }
}
