package com.nvm.minh.androidmock.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.constant.APIConstant
import com.nvm.minh.androidmock.database.DatabaseOpenHelper
import com.nvm.minh.androidmock.listener.ReminderListener
import com.nvm.minh.androidmock.model.Movie
import com.squareup.picasso.Picasso

class ReminderAdapter(
    private var reminderList: ArrayList<Movie>,
    private var type: Int,
    private var databaseOpenHelper: DatabaseOpenHelper
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val REMINDER_ALL = 1
        const val REMINDER_PROFILE = 0
    }
    private lateinit var mReminderListener: ReminderListener

    fun setReminderListener(reminderListener: ReminderListener) {
        this.mReminderListener = reminderListener
    }

    fun updateData(listMovieReminder: ArrayList<Movie>) {
        this.reminderList = listMovieReminder
        notifyDataSetChanged()
    }

    fun deleteItem(index: Int) {
        val movie = reminderList[index]
        if (databaseOpenHelper.deleteReminder(movie.id) > -1) {
            reminderList.removeAt(index)
            notifyDataSetChanged()
        }
        //reminderList.removeAt(index)
    }

    override fun getItemCount(): Int {
        return if (type == REMINDER_ALL) {
            reminderList.size
        } else {
            if (reminderList.size > 3) {
                3
            } else {
                reminderList.size
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ReminderViewHolder(
                type,
                mReminderListener,
                LayoutInflater.from(parent.context).inflate(R.layout.reminder_item, parent, false)
            )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ReminderViewHolder).bindData(position, reminderList[position])
    }

    class ReminderViewHolder(private var reminderType: Int, private var mReminderListener: ReminderListener, itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var imgPoster: ImageView = itemView.findViewById(R.id.r_img_poster)
        private var title: TextView = itemView.findViewById(R.id.r_title_text)
        private var reminder: TextView = itemView.findViewById(R.id.r_reminder_text)
        private var rate_release: TextView = itemView.findViewById(R.id.r_rate_release_text)

        fun bindData(position: Int, movie: Movie) {
            if (reminderType == REMINDER_PROFILE) {
                imgPoster.visibility = View.GONE
            } else {
                val url = APIConstant.BASE_IMG_URL + movie.posterPath
                Picasso.get().load(url).into(imgPoster)
                itemView.setOnClickListener {
                    mReminderListener.onShowDetailReminder(movie)
                }
                itemView.setOnLongClickListener {
                    mReminderListener.onLongClickReminder(position)
                }
            }
            title.text = movie.title
            //release.text = movie.releaseDate
            "Reminder time: ${movie.reminderTimeDisplay}".also { reminder.text = it }
            "Rate: ${movie.voteAverage}/10  Release: ${movie.releaseDate}".also { rate_release.text = it }

        }
    }
}