package com.nvm.minh.androidmock.listener

import com.nvm.minh.androidmock.model.Movie

interface DetailListener {
    fun onUpdateFromDetail(movie: Movie, isFavorite: Boolean)
    fun onAddReminder()
}
