package com.nvm.minh.androidmock.listener

interface BadgeListener {
    fun onUpdateBadgeNumber(isFavourite: Boolean)
}