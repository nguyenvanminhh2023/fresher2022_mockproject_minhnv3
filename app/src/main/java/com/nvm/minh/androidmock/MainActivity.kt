package com.nvm.minh.androidmock

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.nvm.minh.androidmock.adapters.ReminderAdapter
import com.nvm.minh.androidmock.adapters.ViewPagerAdapter
import com.nvm.minh.androidmock.constant.APIConstant.Companion.PREFS_NAME
import com.nvm.minh.androidmock.constant.Constant
import com.nvm.minh.androidmock.database.DatabaseOpenHelper
import com.nvm.minh.androidmock.databinding.ActivityMainBinding
import com.nvm.minh.androidmock.listener.*
import com.nvm.minh.androidmock.model.Movie
import com.nvm.minh.androidmock.util.BitmapConverter
import com.nvm.minh.androidmock.view.*
import java.lang.Exception

class MainActivity : AppCompatActivity(), BadgeListener, MovieListener,
    FavoriteListener, DetailListener, ReminderListener, ProfileListener, SettingListener {
    private var imgBitmap: Bitmap? = null
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    private lateinit var mViewPager: ViewPager
    private lateinit var mTabLayout: TabLayout
    private lateinit var mDrawerLayout: DrawerLayout
    private lateinit var mNavigationView: NavigationView
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var mTabIconList: MutableList<Int>
    private lateinit var mViewPagerAdapter: ViewPagerAdapter

    private lateinit var mMovieFragment: MovieFragment
    private lateinit var mHomeFragment: HomeFragment
    private lateinit var mFavoriteFragment: FavoriteFragment
    private lateinit var mAboutFragment: AboutFragment
    private lateinit var mSettingsFragment: SettingsFragment
    //private lateinit var mDetailFragment: DetailFragment
    private lateinit var mEditProfileFragment: EditProfileFragment
    private lateinit var mReminderFragment: ReminderFragment

    private lateinit var mBadgeNotificationText: TextView
    private lateinit var mDatabaseOpenHelper: DatabaseOpenHelper
    private lateinit var mMovieFavoriteList: ArrayList<Movie>
    private lateinit var mMovieReminderList: ArrayList<Movie>

    private lateinit var mMovieCategory: String
    private lateinit var mRating: String
    private lateinit var mReleaseYear: String
    private lateinit var mSort: String

    //Navigation view
    private lateinit var mEditBtn: Button
    private lateinit var mAvatarImg: ImageView
    private lateinit var mHeaderLayout: View
    private lateinit var mShowAllReminderBtn: Button
    private lateinit var mNameText: TextView
    private lateinit var mEmailText: TextView
    private lateinit var mDateOfBirthText: TextView
    private lateinit var mGenderText: TextView
    private lateinit var mSharedPreferences: SharedPreferences

    private var mFavoriteCount: Int = 0

    private val imgConverter: BitmapConverter = BitmapConverter()

    //Reminder Recycler View
    private lateinit var mReminderAdapter: ReminderAdapter
    private lateinit var mReminderRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        mTabIconList = mutableListOf(
            R.drawable.ic_home,
            R.drawable.ic_favorite,
            R.drawable.ic_settings,
            R.drawable.ic_about
        )
        getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        //load data settings
        mSharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
        mMovieCategory = PreferenceManager.getDefaultSharedPreferences(this).getString(Constant.PREF_CATEGORY_KEY, "popular").toString()
        mRating = PreferenceManager.getDefaultSharedPreferences(this).getInt(Constant.PREF_RATE_KEY, 0).toString()
        mReleaseYear = PreferenceManager.getDefaultSharedPreferences(this).getString(Constant.PREF_RELEASE_KEY, "").toString()
        mSort = PreferenceManager.getDefaultSharedPreferences(this).getString(Constant.PREF_SORT_KEY, "").toString()

        mDatabaseOpenHelper = DatabaseOpenHelper(this, "movie_database", null, 1)
        mMovieFavoriteList = mDatabaseOpenHelper.getListMovie()
        mMovieReminderList = mDatabaseOpenHelper.getListReminder()
        this.mFavoriteCount = mMovieFavoriteList.size

        //Fragment
        mMovieFragment = MovieFragment.newInstance(1)
        mMovieFragment.setCategorySettings(mMovieCategory)
        mMovieFragment.setRateSettings(mRating)
        mMovieFragment.setReleaseYearSettings(mReleaseYear)
        mMovieFragment.setSortSettings(mSort)
        mHomeFragment = HomeFragment(mMovieFragment)
        mFavoriteFragment = FavoriteFragment(mDatabaseOpenHelper, mMovieFavoriteList)
        mEditProfileFragment = EditProfileFragment()
        mSettingsFragment = SettingsFragment()
        mAboutFragment = AboutFragment()

        //Fragment callback listener
        mMovieFragment.setBadgeListener(this)
        mMovieFragment.setMovieListener(this)
        mMovieFragment.setDetailListener(this)
        mFavoriteFragment.setBadgeListener(this)
        mFavoriteFragment.setFavoriteListener(this)
        mEditProfileFragment.setProfileListener(this)
        mSettingsFragment.setSettingListener(this)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        mDrawerLayout = binding.drawerLayout
        mNavigationView = binding.navigationView
        val navController = findNavController(R.id.fragment_content_main)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_favorite, R.id.nav_settings, R.id.nav_about
            ), mDrawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        mNavigationView.setupWithNavController(navController)

        //set up drawer layout
        toggle = ActionBarDrawerToggle(this, mDrawerLayout, binding.appBarMain.toolbar, R.string.open,R.string.close)
        mDrawerLayout!!.addDrawerListener(toggle)
        mHeaderLayout = mNavigationView.getHeaderView(0)

        setUpTabs()

        //Navigation view items
        mEditBtn = mHeaderLayout.findViewById(R.id.edit_profile_btn)
        mShowAllReminderBtn = mHeaderLayout.findViewById(R.id.show_reminder_btn)
        mAvatarImg = mHeaderLayout.findViewById(R.id.img_avatar)
        mNameText = mHeaderLayout.findViewById(R.id.tv_name)
        mDateOfBirthText = mHeaderLayout.findViewById(R.id.dob_tv)
        mEmailText = mHeaderLayout.findViewById(R.id.email_tv)
        mGenderText = mHeaderLayout.findViewById(R.id.gender_tv)

        // Navigation event
        loadDataProfile()
        mEditBtn.setOnClickListener {
//            if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
//                this.mDrawerLayout.closeDrawer(GravityCompat.START)
//            }
            val bundle = Bundle()
            bundle.putString("name", mNameText.text.toString())
            bundle.putString("email", mEmailText.text.toString())
            bundle.putString("dob", mDateOfBirthText.text.toString())
            bundle.putString("gender", mGenderText.text.toString())
            try {
                bundle.putString(
                    "imgBitmapString",
                    mSharedPreferences.getString("profileImg", "No data")
                )
            } catch (e: Exception) {
            }
            Toast.makeText(this, "Opening Edit Profile", Toast.LENGTH_SHORT).show()
            mEditProfileFragment.arguments = bundle
            if (!mEditProfileFragment.isAdded) {
                supportFragmentManager.beginTransaction().apply {
                    add(R.id.relative_layout, mEditProfileFragment)
                    addToBackStack(null)
                    commit()
                }
            }
        }
        mShowAllReminderBtn.setOnClickListener {
            mReminderFragment = ReminderFragment(mDatabaseOpenHelper)
            mReminderFragment.setReminderListener(this)
            supportFragmentManager.beginTransaction().apply {
                add(R.id.relative_layout, mReminderFragment, Constant.FRAGMENT_REMINDER_TAG)
                addToBackStack(null)
                commit()
            }
        }

        createNotificationChannel()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private var menu: Menu? = null

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.change_layout -> {
                mMovieFragment.changeViewHome()
                changeItemSetView()
            }
            R.id.action_home -> {
                mViewPager.currentItem = 0
            }
            R.id.action_favorite -> {
                mViewPager.currentItem = 1
            }
            R.id.action_settings -> {
                mViewPager.currentItem = 2
            }
            R.id.action_info -> {
                mViewPager.currentItem = 3
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun changeItemSetView() {
        //menu?.getItem(4)?.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_launcher));
        if (mMovieFragment.getScreenType() == 1) {
            menu?.getItem(4)?.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_baseline_apps_24));
        } else {
            menu?.getItem(4)?.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_baseline_format_list_bulleted_24));
        }
    }

    private var mTabTitleList = arrayOf("Movies", "Favorite", "Settings", "About")

    private fun setUpTabs() {
        mReminderRecyclerView = mHeaderLayout.findViewById(R.id.recycler_view_reminder)
        mReminderAdapter = ReminderAdapter(mMovieReminderList, ReminderAdapter.REMINDER_PROFILE, mDatabaseOpenHelper)
        mReminderAdapter.setReminderListener(this)
        val linearLayoutManager = LinearLayoutManager(this)
        mReminderRecyclerView.layoutManager = linearLayoutManager
        mReminderRecyclerView.setHasFixedSize(true)
        mReminderRecyclerView.adapter = mReminderAdapter

        mViewPager = findViewById(R.id.viewPager)
        mTabLayout = findViewById(R.id.tabs)

        mViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        mViewPagerAdapter.addFragment(mHomeFragment, "Movies")
        mViewPagerAdapter.addFragment(mFavoriteFragment, "Favorite")
        mViewPagerAdapter.addFragment(mSettingsFragment, "Settings")
        mViewPagerAdapter.addFragment(mAboutFragment, "About")

        mViewPager.offscreenPageLimit = 4
        mViewPager.adapter = mViewPagerAdapter
        mViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }
            override fun onPageSelected(position: Int) {
                if (position > 1) {
                    for (i in 0 until supportFragmentManager.backStackEntryCount) {
                        supportFragmentManager.popBackStack()
                    }
                }
            }
            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        mTabLayout.setupWithViewPager(mViewPager)

        val countFragment = mViewPagerAdapter.count
        for (i in 0 until countFragment) {
            //mTabLayout.getTabAt(i)!!.setIcon(mIconList[i])
            if (i == 1) {
                mTabLayout.getTabAt(i)!!.setCustomView(R.layout.tab_item_favorite)
                mTabLayout.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.count_fav).setText(this.mFavoriteCount.toString())
            } else {
                mTabLayout.getTabAt(i)!!.setCustomView(R.layout.tab_item)
                mTabLayout.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.title_tab).setText(mTabTitleList[i])
                mTabLayout.getTabAt(i)!!.customView!!.findViewById<ImageView>(R.id.image).setImageResource(mTabIconList[i])
            }
        }
        mBadgeNotificationText = mTabLayout.getTabAt(1)!!.customView!!.findViewById(R.id.count_fav)

        setTitleFragment()
    }

    private fun setTitleFragment() {
        mViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }
            override fun onPageSelected(position: Int) {
                mTabLayout.nextFocusRightId = position
                supportActionBar?.title = mTabTitleList[position]
            }
            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

    private fun createNotificationChannel() {
        val channel = NotificationChannel(channelID, "Movie!!!", NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = "Time to watch a movie <3"
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    override fun onUpdateBadgeNumber(isFavourite: Boolean) {
        val tabView = mTabLayout.getTabAt(1)?.customView
        val badgeText = tabView?.findViewById<TextView>(R.id.count_fav)
        if (isFavourite) {
            badgeText?.setText("${++mFavoriteCount}", TextView.BufferType.EDITABLE)
        }
        else badgeText?.setText("${--mFavoriteCount}", TextView.BufferType.EDITABLE)
    }

    override fun onUpdateFromFavorite(movie: Movie) {
        mMovieFragment.updateMovieList(movie, false)
        val detailFragment = supportFragmentManager.findFragmentByTag(Constant.FRAGMENT_DETAIL_TAG)
        if (detailFragment != null) {
            detailFragment as DetailFragment
            detailFragment.unFavoriteMovie(movie.id)
        }
        mHomeFragment.setMovieFragment(mMovieFragment)
    }

    override fun onUpdateFromMovie(movie: Movie, isFavorite: Boolean) {
        mFavoriteFragment.updateFavoriteList(movie, isFavorite)
    }

    override fun onUpdateTitleMovie(movieTitle: String) {
        supportActionBar?.title = movieTitle
    }

    override fun onUpdateFromDetail(movie: Movie, isFavorite: Boolean) {
        mMovieFragment.updateMovieList(movie, isFavorite)
        mHomeFragment.setMovieFragment(mMovieFragment)
        mFavoriteFragment.updateFavoriteList(movie, isFavorite)
    }

    override fun onAddReminder() {
        mMovieReminderList = mDatabaseOpenHelper.getListReminder()
        mReminderAdapter.updateData(mMovieReminderList)
        //mReminderAdapter.notifyDataSetChanged()
    }

    override fun onLoadReminder() {
        mMovieReminderList = mDatabaseOpenHelper.getListReminder()
        mReminderAdapter = ReminderAdapter(mMovieReminderList, ReminderAdapter.REMINDER_PROFILE, mDatabaseOpenHelper)
        mReminderAdapter.setReminderListener(this)
        val linearLayoutManager = LinearLayoutManager(this)
        mReminderRecyclerView.layoutManager = linearLayoutManager
        mReminderRecyclerView.setHasFixedSize(true)
        mReminderRecyclerView.adapter = mReminderAdapter
    }

    override fun onShowDetailReminder(movie: Movie) {
        mViewPager.currentItem = 0
        supportActionBar!!.title = "Movie"

        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START)
        }

        val reminderCurrentFragment = supportFragmentManager.findFragmentByTag(Constant.FRAGMENT_REMINDER_TAG)
        if (reminderCurrentFragment != null) {
            supportFragmentManager.beginTransaction().apply {
                remove(reminderCurrentFragment)
                commit()
            }
        }
        val detailCurrentFragment = supportFragmentManager.findFragmentByTag(Constant.FRAGMENT_DETAIL_TAG)
        if (detailCurrentFragment != null) {
            supportFragmentManager.beginTransaction().apply {
                remove(detailCurrentFragment)
                commit()
            }
        }

        val bundle = Bundle()
        bundle.putSerializable("movieDetail", movie)
        val detailFragment = DetailFragment()
        detailFragment.setDatabaseOpenHelper(mDatabaseOpenHelper)
        detailFragment.setBadgeListener(this)
        detailFragment.setDetailListener(this)
        detailFragment.setMovieListener(this)
        detailFragment.arguments = bundle
        supportFragmentManager.beginTransaction().apply {
            add(R.id.frg_home, detailFragment, Constant.FRAGMENT_DETAIL_TAG)
            addToBackStack(null)
            commit()
        }
    }

    override fun onLongClickReminder(position: Int): Boolean {
        TODO("Not yet implemented")
    }

    override fun onSaveProfile(
        name: String,
        email: String,
        dob: String,
        gender: String,
        imgBitmap: Bitmap?
    ) {
        val edit = mSharedPreferences.edit()
        edit.putString("profileName", name)
        edit.putString("profileEmail", email)
        edit.putString("profileDob", dob)
        edit.putString("profileGender", gender)
        if (imgBitmap != null) {
            edit.putString("profileImg", imgConverter.encodeBase64(imgBitmap))
        }
//        this.imgBitmap = imgBitmap
//        mAvatarImg.setImageBitmap(imgBitmap)
        edit.apply()
        loadDataProfile()

        Toast.makeText(this, "Save profile successfully", Toast.LENGTH_SHORT).show()
    }

    private fun loadDataProfile() {
        mNameText.text = mSharedPreferences.getString("profileName", "No data")
        mEmailText.text = mSharedPreferences.getString("profileEmail", "No data")
        mDateOfBirthText.text = mSharedPreferences.getString("profileDob", "No data")
        mGenderText.text = mSharedPreferences.getString("profileGender", "No data")

        try {
            mAvatarImg.setImageBitmap(
                imgConverter.decodeBase64(
                    mSharedPreferences.getString("profileImg", "No data")
                )
            )
        } catch (e : Exception) {
            mAvatarImg.setImageResource(R.drawable.ic_baseline_person_24)
        }
    }

    override fun onUpdateFromSettings(
        category: String,
        rate: String,
        releaseYear: String,
        sort: String
    ) {
        mMovieFragment.setListMovieByCondition(category, rate, releaseYear, sort)
    }
}