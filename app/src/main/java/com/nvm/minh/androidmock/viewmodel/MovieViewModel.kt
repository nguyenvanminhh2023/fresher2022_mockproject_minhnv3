package com.nvm.minh.androidmock.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nvm.minh.androidmock.model.Movie

class MovieViewModel : ViewModel() {
    var movieList: MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    var movieListDB: MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    var movieFavoriteNumber: MutableLiveData<Int> = MutableLiveData()

    init {
        movieList.value = ArrayList()
        movieListDB.value = ArrayList()
        movieFavoriteNumber.value = 0
    }
}