package com.nvm.minh.androidmock.view

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.nvm.minh.androidmock.R
import com.nvm.minh.androidmock.listener.ProfileListener
import com.nvm.minh.androidmock.util.BitmapConverter
import java.lang.Exception
import java.util.*

class EditProfileFragment : Fragment() {
    private lateinit var mProfileImg: ImageView
    private lateinit var mRadioGroup: RadioGroup
    private lateinit var mRadioMale: RadioButton
    private lateinit var mRadioFemale: RadioButton
    private lateinit var mNameEdit: EditText
    private lateinit var mEmailEdit: EditText
    private lateinit var mDateOfBirthText: TextView
    private lateinit var mSaveBtn: Button

    private var mBitmapProfile: Bitmap? = null
    private val mConverterImg: BitmapConverter = BitmapConverter()
    private var mGender: String = ""

    private var mSaveDay = 0
    private var mSaveMonth = 0
    private var mSaveYear = 0

    private val REQUEST_IMAGE_CAPTURE = 1

    private lateinit var mProfileListener: ProfileListener

    fun setProfileListener(profileListener: ProfileListener) {
        this.mProfileListener = profileListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_edit_profile, container, false)

        mRadioGroup = view.findViewById(R.id.rd_group)
        mRadioMale = view.findViewById(R.id.radio_male)
        mRadioFemale = view.findViewById(R.id.radio_female)
        onClickChanged()

        mProfileImg = view.findViewById(R.id.img_profile)
        mNameEdit = view.findViewById(R.id.ed_name)
        mEmailEdit = view.findViewById(R.id.ed_email)
        mDateOfBirthText = view.findViewById(R.id.ed_dob)
        mSaveBtn = view.findViewById(R.id.save_btn)

        val bundle = arguments
        if (bundle != null) {
            var nameBundle = bundle.getString("name")
            var emailBundle = bundle.getString("email")
            var dobBundle = bundle.getString("dob")
            if (nameBundle == "No data") {
                nameBundle = ""
            }
            if (emailBundle == "No data") {
                emailBundle = ""
            }
            if (dobBundle == "No data") {
                dobBundle = ""
            }

            mNameEdit.setText(nameBundle, TextView.BufferType.EDITABLE)
            mEmailEdit.setText(emailBundle, TextView.BufferType.EDITABLE)
            mDateOfBirthText.setText(dobBundle, TextView.BufferType.EDITABLE)

            try {
                mProfileImg.setImageBitmap(mConverterImg.decodeBase64(bundle.getString("ImgBitmapString")))
            } catch (e: Exception) {
                mProfileImg.setImageResource(R.drawable.ic_baseline_person_24)
            }
            if (bundle.getString("gender").toString().contains("Female")) {
                mRadioGroup.check(R.id.radio_female)
            } else {
                mRadioGroup.check(R.id.radio_male)
            }
        }

        mDateOfBirthText.setOnClickListener { setDate() }

        mSaveBtn.setOnClickListener {
            val name = mNameEdit.text.toString()
            val email = mEmailEdit.text.toString()
            val dob = mDateOfBirthText.text.toString()
            if (name == "" || email == "" || dob == "") {
                Toast.makeText(context, "Fill all information!", Toast.LENGTH_SHORT).show()
            } else {
                mProfileListener.onSaveProfile(name, email, dob, mGender, mBitmapProfile)
                activity?.supportFragmentManager!!.beginTransaction().remove(this).commit()
            }
        }

        mProfileImg.setOnClickListener { dispatchTakePictureIntent() }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imgBitmap = data!!.extras!!.get("data") as Bitmap
            this.mBitmapProfile = imgBitmap
            mProfileImg.setImageBitmap(imgBitmap)
        }
    }

    private fun onClickChanged() {
        mRadioGroup.setOnCheckedChangeListener { _, i ->
            if (i == R.id.radio_male) {
                mGender = "Male"
            } else if (i == R.id.radio_female) {
                mGender = "Female"
            }
        }
    }

    private fun setDate() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)

        DatePickerDialog(requireContext(), { _, year, month, day ->
            val pickedDatetime = Calendar.getInstance()
            pickedDatetime.set(year, month, day)
            mSaveYear = year
            mSaveMonth = month
            mSaveDay = day
            currentDateTime.set(mSaveYear, mSaveMonth, mSaveDay)
            val timeHour = "$mSaveYear-${mSaveMonth + 1}-$mSaveDay"
            mDateOfBirthText.setText(timeHour, TextView.BufferType.EDITABLE)
        }, startYear, startMonth, startDay).show()
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: Exception) {
            //display error
        }
    }
}