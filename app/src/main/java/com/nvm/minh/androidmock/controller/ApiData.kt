package com.nvm.minh.androidmock.controller

import com.nvm.minh.androidmock.adapters.MovieAdapter
import com.nvm.minh.androidmock.api.ApiInterface
import com.nvm.minh.androidmock.api.RetrofitClient
import com.nvm.minh.androidmock.constant.APIConstant
import com.nvm.minh.androidmock.model.Movie
import com.nvm.minh.androidmock.model.MovieList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ApiData {
    fun getListMovieFromApi(
        pageCurrent: Int,
        typeListMovie: String,
        listMovieCurrent: ArrayList<Movie>,
        mMovieListDB: ArrayList<Movie>,
        adapter: MovieAdapter,
    ): Int {
        val page = pageCurrent
        val retrofit : ApiInterface = RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getMovieList(typeListMovie, APIConstant.API_KEY, "$page")
        retrofitData.enqueue(object : Callback<MovieList> {
            override fun onResponse(call: Call<MovieList>?, response: Response<MovieList>?) {
                val responseBody = response?.body()
                val listMovieResult = responseBody?.results as ArrayList<Movie>
                for (i in 0 until listMovieResult.size) {
                    if (!listMovieCurrent.contains(listMovieResult[i])) {
                        listMovieCurrent.add(listMovieResult[i])
                    }
                }
                adapter.updateData(listMovieCurrent)
                adapter.settingMovieFavorite(mMovieListDB)
                adapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<MovieList>?, t: Throwable?) {
            }

        })
        return page
    }
}
